# Run 2 Offline Timing Tool
The tool has two functions: 1) applying timing corrections to data and 2) smearing MC timing to match data. 

1. The corrections apply offline precision timing corrections to layer two cell times in the Liquid Argon (LAr) Calorimeter for electrons and photons. Corrections are available for all runs on the GRLs for 2015-2018 (except for some minor exceptions, see the Intervals of Validity (IOVs) section). See the Event Requirements section for details about other event requirements.

2. Smearing modifies MC timing for electrons and photons to match the resolutions observed in data with timing corrections applied.

## Authors

**Current Contact:** Devin Mahon (devin.mahon@cern.ch)

* Full Run 2 tool & corrections

Jue Chen (jue@cern.ch)

* 2016 developments

Ryne Carbone (ryne.carbone@cern.ch)

* Original tool & 2015 developments

## Setup

### Tool Requirements

* Athena release 21.2.14 or higher

### Install the Package

Download the package from git into the `source` directory of your analysis framework.

```bash
git clone ssh://git@gitlab.cern.ch:7999/dmahon/OfflineTimingToolRun2.git
```

In your analysis' `CMakeLists.txt` file make the following additions: 

```bash
...
atlas_depends_on_subdir (	PRIVATE 			# Add this if not present		
				...
                          	OfflineTimingToolRun2 		# Add this
				...
			)
...
atlas_add_library	(	PRIVATE_LINK_LIBRARIES		# Add this if not present
				...
				OfflineTimingToolRun2Lib	# Add this
				...
			)
...
```

Go to your `build` directory and run `cmake ../source`. Then run `make` to compile.

### Setup the Tool

1. Add the `include` for the tool in your analysis header file (wherever you normally initialize your tools).

```cpp
#include "OfflineTimingTool/OfflineTimingTool.h"
```

Declare an object from the OfflineTimingTool class in the header as well.

```cpp
OfflineTimingTool *m_OfflineTimingTool; //!
```

2. Initalize the tool, setting the desired debug flag.

```cpp
m_OfflineTimingTool = new OfflineTimingTool(false); // Pass true to print detailed debugging info
```

Go to your `build` directory, rerun `make`, and make sure it compiles successfully.

## Timing Corrections Usage
To obtain corrected times, follow the steps below after properly installing the package and setting up the tool as described in the Setup section.

> **Note:** there are restrictions to the availability of timing corrections. See the subsequent sections for details.

1. Get the run number (`rN`) and primary vertex z position (`PV_z`) and ask the tool to compute the correction,
telling the tool if the object is a photon with the second arguement (pass `true` for photons, `false` for electrons). 
Store both return values: `valid` and `t_corrected`.

```cpp
bool valid = false; double t_corrected = -99999;
// Electrons:
tie(valid, t_corrected) = m_OfflineTimingTool->getCorrectedTime(*el, false, rN, PV_z);
// Photons:
tie(valid, t_corrected) = m_OfflineTimingTool->getCorrectedTime(*ph, true, rN, PV_z);
```

2. Check the `valid` flag to see whether or not a valid corrected time could be successfully computed.

```cpp 
if (valid) t = t_corrected; // t can then be used as the valid corrected time
else continue; // i.e. skip the event, as the timing is invalid
``` 

> **Note:** if the corrected time is not valid, `t_corrected` will also be set to -99999.

## Event Requirements
There are a few limitations to the corrections available. The corrections are provided for electrons and photons based on the cell in layer 2 with the maximum energy deposit (maxEcell) provided that the following are true:

* maxEcell is in the barrel (EMB) or endcap (EMEC) and not in the crack region, i.e. `(0 < abs(etas2) < 1.37)  ||  (1.52 < abs(etas2) < 2.47)` (`etas2` is the layer 2 cluster eta).
* maxEcell gain is high (gain = 0) or medium (gain = 1). Low gain does not have corrections due to low statistics in the calibration data set.
* maxEcell does not correspond to a "bad channel", as determined by the calibration procedure and implemented in the tool. "Bad channels" are those with abnormal timing behavior such as timing shifts within an IOV. There are very few such channels: ~12 out of more than 45,000 channels throughout all of Run 2.
* Event is on the Run 2 GRLs except for runs 325713 and 325790 (see the Intervals of Validity (IOVs) section for more details).
* There are enough statistics to provide reliable corrections (automatically determined by the calibration procedure and implemented by the tool).
* The electron or photon has the appropriately formatted variables associated with it (see the Expected Variables section below for a complete list).

## Expected Variables
The following variables are expected to be part of the Electron or Photon object (represented by `el` below):

* `el->auxdata<unsigned long int>("maxEcell_onlId")`
* `el->auxdata<int>("maxEcell_gain")`
* `el->auxdata<float>("maxEcell_energy")`
* `el->auxdata<float>("maxEcell_time")`
* `el->auxdata<float>("maxEcell_x")`
* `el->auxdata<float>("maxEcell_y")`
* `el->auxdata<float>("maxEcell_z")`
* `el->auxdata<float>("f1")`
* `el->auxdata<float>("f3")`
* `el->auxdata<float>("caloCluster_e")`
* `el->auxdata<float>("caloCluster_etas2")`
* `el->auxdata<float>("caloCluster_phis2")`

---
## Invervals of Validity (IOVs)

Channel correction are calculated separately for each IOV, i.e. a period of runs in which the Optimal Filtering Coefficients (OFCs) used to compute the energy and time from LAr ionization pulses are constant for all Front End Boards (FEBs). Changes to these OFCs are normally adjusted 3-5 times per year to compensate for observed changes in calibration data. 

For all of Run 2 there are 13 valid IOVs. The tool automatically determines which IOV corrections to use based on the run number.

There are 2 runs on the GRLs for Run 2 for which corrections are not available. This is because these 2 runs alone comprise an entire IOV, which does not contain enough statstics to compute valid corrections. These 2 runs correspond to low-luminosty runs during the ramp-up of 2017 data-taking during which the OFCs were still being fine-tuned. Thus, the tool will return an invalid flag for the following 2 runs:

* 325713
* 325790

The GRL version used in the calibration are listed below:

* `data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml`
* `data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml`
* `data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml`
* `data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml`

See the [GoodRunListsForAnalysisRun2 twiki](https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/GoodRunListsForAnalysisRun2) for more information.

---
## MC Time Smearing
Smearing can be applied to electron and photon times in MC to match the observed resolutions in data. Smearing is only available for high and medium gains. 

### Smearing Information

The smearing consists of taking the raw time from MC, which already contains a model of the observed noise in the calorimeter, and adding additional correlated and uncorrelated componenets.
The correlated component corresponds to the beam spread, and objects in the same event share this correlated collision time.
The uncorrelated component is applied separately to each objects and corresponds to different electronic and pileup noise assumptioins and other global effects observed in timing studies compared to noise studies done for MC.

The smearing relies on the data used to study and calibrate the timing resolutions. Thus, smearing is only available for electros and photons with the following properties: 

* maxEcell gain is high (gain = 0) or medium (gain = 1).
* maxEcell is in the barrel (EMB) or endcap (EMEC) and not in the crack region, i.e. `(0 < abs(etas2) < 1.37)  ||  (1.52 < abs(etas2) < 2.47)` (`etas2` is the layer 2 cluster eta).

### Smearing Usage

To obtain smeared times, follow the steps below after properly installing the package and setting up the tool as described in the Setup section. 

The normal configuration for using the tool is an Athena-based event loop, but it can also be run on a flat ntuple.

> **For running on flat ntuples:**
>
> For information on how to run on a flat ntuple without an Athena environment, see the accompanying blockquoted  notes (like these lines) below.

1. In the event loop, outside of the electron/photon loop, declare a `double` to store a correlated collision time and a `bool` to store a flag to keep track of correlated times within the event. 
Here you can also set the smearing mode to smear with the nominal amount or with the recommended up and down smearing systematics.
To get the nominal amound of smearing, i.e. that which most closely matches data, set `mode =  0`, as below. 
For the up and down systematic smearing, set `mode = 1` or `mode = -1`, respectively. 

> **Note:** the systematic smearing variations are only available in the barrel. 
> Here, matching of MC and data is done carefully by slot in an energy-dpendent way. 
> The smearing in the endcaps is done more crudely with a flat uncorrelated component across all slots and energies. 

> **Note:** The smearing systematics modify only the uncorrelated component of the time resolution. 
> The beam spread component is measured much more precisely such that its uncertainty is negligible in comparison. 

```cpp
double t_coll = 0.0;
bool corr = false;
int mode = 0; // 0 for nominal smearing, 1 for up systematic, -1 for down systematic
```

2. In the electron/photon loop, get the primary vertex z position (`PV_z`). Then get the smeared time and set `corr` to `true`.

```cpp
float PV_z = el.auxdata<float>("PV_z");
bool valid = false; double t_smeared = -99999;
tie(valid, t_smeared) = m_OfflineTimingTool->getSmearedTime(*el, PV_z, t_coll, corr, mode);
corr = true;
```

> **Note:** the first electron/photon in the event will have `corr = false`, which causes the tool to generate a collision time that is then stored in `t_coll`.
> Then all subsequent electrons/photons in the event will have `corr = true`, which tells the tool to use `t_coll` for the collision time.
> So all objects in the same even will share the same collision time.

> **For running on flat ntuples:**
>
> Instead of the `Electron` and `Photon` objects, create a `caloObject_t` object as follows:
> ```cpp
> OfflineTimingTool::caloObject_t caloObj;
> 
> caloObj.m_rn     = this->_event->runNumber;
> caloObj.m_onlId  = this->_event->ph_maxEcell_onlId->at(phIdx);
> caloObj.m_onlId  = caloObj.m_onlId >> 32; //Is stored as 64bit, last 32 bits area zero
> caloObj.m_gain   = this->_event->ph_maxEcell_gain->at(phIdx);
> caloObj.m_energy = this->_event->ph_maxEcell_energy->at(phIdx)/1.e3;
> caloObj.m_time   = this->_event->ph_maxEcell_time->at(phIdx);
> caloObj.m_x      = this->_event->ph_maxEcell_x->at(phIdx);
> caloObj.m_y      = this->_event->ph_maxEcell_y->at(phIdx);
> caloObj.m_z      = this->_event->ph_maxEcell_z->at(phIdx);
> caloObj.m_f1     = this->_event->ph_f1->at(phIdx);
> caloObj.m_f3     = this->_event->ph_f3->at(phIdx);
> caloObj.m_etas2  = this->_event->ph_etas2->at(phIdx);
> caloObj.m_phis2  = this->_event->ph_phis2->at(phIdx);
> caloObj.m_PV_z   = this->_event->vx_z->at(0);
> caloObj.m_corrected_time = this->_event->el_maxEcell_time->at(phIdx);
> caloObj.valid    =true;
> ```
> 
> Then do the following: 
> 
> ```cpp
> bool valid = false; double t_smeared = -99999;
> m_OfflineTimingTool->getSmearedTime(caloObj, t_coll, corr, mode);
> corr = true;
> valid = caloObj.valid;
> t_smeared = caloObj.m_corrected_time;
> ```

3. Check the `valid` flag to see whether or not a valid smeared time could be successfully computed. If not, `t_smeared` will contain the original, raw time.

```cpp
if (valid) t = t_smeared; // t can then be used as the valid smeared time
else continue; // i.e. skip the event (or use the raw, unsmeared time), as the proper smearing could not be performed
```
