################################################################################
# Package: OfflineTimingTool
################################################################################

# Declare the package name:
atlas_subdir( OfflineTimingTool )

# Extra dependencies, based on the build environment:
set( extra_deps )
if( NOT XAOD_STANDALONE )
   set( extra_deps Control/AthenaBaseComps GaudiKernel )
endif()

# Declare the package's dependencies:
atlas_depends_on_subdirs(
   PUBLIC
   Event/xAOD/xAODEgamma
   Event/xAOD/xAODEventInfo
   Event/xAOD/xAODTracking
   EventLoop
   EventLoopAlgs
   xAODRootAccess
   Trigger/TrigAnalysis/TrigAnalysisInterfaces
   Trigger/TrigAnalysis/TrigBunchCrossingTool
   PRIVATE
   Tools/PathResolver
   ${extra_deps} )

# External dependencies:
find_package( ROOT COMPONENTS Core Tree Hist RIO Boost Graf Gpad)

atlas_add_root_dictionary( OfflineTimingToolRun2Lib
   OfflineTimingToolDictSource
   ROOT_HEADERS OfflineTimingTool/*.h Root/LinkDef.h
   EXTERNAL_PACKAGES ROOT )

# Libraries in the package:
atlas_add_library( OfflineTimingToolRun2Lib
   OfflineTimingTool/*.h OfflineTimingTool/*.inh Root/*.cxx ${OfflineTimingToolDictSource}
   PUBLIC_HEADERS OfflineTimingTool
   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
   LINK_LIBRARIES ${ROOT_LIBRARIES} EventLoop TrigBunchCrossingTool PathResolver)

atlas_install_data( data/* )

if( NOT XAOD_STANDALONE )
   atlas_add_component( OfflineTimingTool
      src/*.h src/*.cxx src/components/*.cxx
      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
      LINK_LIBRARIES ${ROOT_LIBRARIES} AthContainers AthenaBaseComps
      GaudiKernel OfflineTimingToolRun2Lib )
endif()

# Test(s) in the package:
if( XAOD_STANDALONE )
   atlas_add_test( Local_Test_OfflineTimingTool
      SOURCES util/Local_Test_OfflineTimingTool.cxx
      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
      LINK_LIBRARIES ${ROOT_LIBRARIES} xAODEgamma xAODEventInfo xAODTracking EventLoop OfflineTimingToolRun2Lib )
   atlas_add_test( Photon_Test_OfflineTimingTool
      SOURCES util/Photon_Test_OfflineTimingTool.cxx
      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
      LINK_LIBRARIES ${ROOT_LIBRARIES} xAODEgamma xAODEventInfo xAODTracking EventLoop OfflineTimingToolRun2Lib )
   atlas_add_test( Smear_Test_OfflineTimingTool
      SOURCES util/Smear_Test_OfflineTimingTool.cxx
      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
      LINK_LIBRARIES ${ROOT_LIBRARIES} xAODEgamma xAODEventInfo xAODTracking EventLoop OfflineTimingToolRun2Lib )
endif()
